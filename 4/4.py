def reverse_float(n):
    parts = str(n).split('.')
    rev_parts = [''.join(reversed(part)) for part in parts]

    return ('.'.join(rev_parts))

num = float(input("Input num: "))
num2 = float(input("Input num2: "))


rev_num = float(reverse_float(num))
rev_num2 = float(reverse_float(num2))

print("Revers: ", rev_num)
print("Revers2: ", rev_num2)
print("Sum: ", rev_num + rev_num2)
print("Введите первую точку")
x1 = float(input('X: '))
y1 = float(input('Y: '))
print("\nВведите вторую точку")
x2 = float(input('X: '))
y2 = float(input('Y: '))

x_diff = x1 - x2
y_diff = y1 - y2
if (x_diff != 0):
    k = y_diff / x_diff
    b = y2 - k * x2

    print("Уравнение прямой, проходящей через эти точки:")
    print("y = ", k, " * x + ", b)

else:
    x_vec = x2 - x1
    y_vec = y2 - y1
    print("Параметрическое уравнение прямой на плоскости:")
    print("x =",x_vec,"t+",x1)
    print("y =",y_vec,"t+",y1)

    print("Данная прямая параллельна оси OY, а ее уравнение можно записать в виде:")
    print("x =",x1)
